<?php

const MAX_NUMBER = 1_000_000;
const MIN_NUMBER = 0;
const MIN_PRIME_NUMBER = 2;

function isPrime(int $number): bool
{
    if ($number < 2) {
        return false;
    }

    for ($i = 2; $i <= \sqrt($number); $i++) {
        if ($number % $i === 0) {
            return false;
        }
    }

    return true;
}

function nextPrimeNumber(int $number): int
{
    $number++;

    while (!isPrime($number)) {
        $number++;
    }

    return $number;
}

function previousPrimeNumber(int $number): int
{
    $number--;

    while (!isPrime($number)) {
        $number--;
    }

    return $number;
}

function isValidNumber($number): bool
{
    return isset($number) === false || !is_numeric($number) || $number < MIN_NUMBER || $number > MAX_NUMBER;
}
