<?php
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prime Nums</title>
</head>
<body>
    <form action="../index.php" method="post">
        <label>
            <input type="number" min="0" max="1000000" name="number" placeholder="Enter a number">
        </label>
        <button type="submit">Check</button>
    </form>
</body>
</html>
