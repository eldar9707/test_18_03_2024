<?php

include_once 'templates/template.php';
include_once 'functions/functions.php';

$number = $_POST['number'] ?? null;

if (isValidNumber($number)) {
    exit;
}

$number = (int) $number;

if (isPrime($number)) {
    echo 'The number is prime' . PHP_EOL;
} else {
    echo 'The number is not prime. Next prime number: ' . nextPrimeNumber($number);

    if ($number > MIN_PRIME_NUMBER) {
        echo '. Previous prime number: ' . previousPrimeNumber($number);
    }
}
